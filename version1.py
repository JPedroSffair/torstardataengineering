# 1 - Number of June 2016 Posts: 30727
# 2 - Posts Tagged with combinatorics: 33394
# 2.a - Posts Tagged with combinatorics but not fibonacci-numbers: 33270
# 3 - Most popular month with graph-theory: November (11)
# 4 - Timeseries plot of quarterly graph-theory posts:
# Time for all tasks: 51.15s

import os
import time
import array
import calendar
import math
import matplotlib.pylab as plt
from io import StringIO, BytesIO
from lxml import etree

# Functions
def defineQuarter(YYYYMMDate):
    year = int(YYYYMMDate.split('-', 1)[0])
    month = int(YYYYMMDate.split('-', 3)[1])
    return "Q{}{}".format(math.ceil(month/3), year)

start = time.time()
file_name = "Posts.xml"
full_file = os.path.abspath(os.path.join('data', file_name))

print(full_file)

june2016Posts = 0
combinatoricsPosts = 0
combinatoricsNotFibbonacciPosts = 0
graphTheoryPosts = {}
graphTheoryMonths = array.array('i',(0 for i in range(0,12)))
postsPerQuarter = {}

for event, element in etree.iterparse(full_file, tag="row", encoding="utf-8"):
    creationDate = element.attrib['CreationDate']
    # Posts from June 2016
    if '2016-06' in creationDate:
        june2016Posts += 1

    # Check if there are tags
    commentTags = element.get('Tags')
    if isinstance(commentTags, str):
        if 'combinatorics' in commentTags:
            # Posts tagged with combinatorics
            combinatoricsPosts += 1

            if 'fibonacci-number' not in commentTags:
                # Post tagged with combinatorics but not fibonacci-numbers
                combinatoricsNotFibbonacciPosts += 1

        # Track graph-theory posts
        if 'graph-theory' in commentTags:
            # Count graph-theory tagged posts by month
            month = int(creationDate.split('-', 3)[1])
            graphTheoryMonths[month - 1] += 1

            # Gather posts/per month/per year for timeseries
            dateInYYYYMMFormat = ("{}-{}".format(creationDate.split('-', 1)[0], creationDate.split('-', 3)[1]))
            if not dateInYYYYMMFormat in graphTheoryPosts:
                graphTheoryPosts[dateInYYYYMMFormat] = 1
            else:
                graphTheoryPosts[dateInYYYYMMFormat] += 1
    element.clear()

# Processing
maxAmountOfPostsInAMonth = max(graphTheoryMonths)
mostPopularMonth = graphTheoryMonths.index(maxAmountOfPostsInAMonth) + 1

for post in graphTheoryPosts:
    quarter = defineQuarter(post)
    if not quarter in postsPerQuarter:
        postsPerQuarter[quarter] = 0
    postsPerQuarter[quarter] += graphTheoryPosts[post]

# Plotting
x = list(postsPerQuarter.keys())
y = list(postsPerQuarter.values())
plt.plot(x, y)


# Outputs
if june2016Posts == 30727:
    print("{} {}".format('june2016Posts: ', june2016Posts))
else:
    print("Wrong June 2016 count: {}".format(june2016Posts))
if combinatoricsPosts == 33394:
    print("{} {}".format('combinatoricsPosts: ', combinatoricsPosts))
else:
    print("Wrong Combinatorics only count: {}".format(combinatoricsPosts))
if combinatoricsNotFibbonacciPosts == 33270:
    print("{} {}".format('combinatoricsNotFibbonacciPosts: ',
                         combinatoricsNotFibbonacciPosts))
else:
    print("Wrong combinatoricsNotFibbonacciPosts only count: {}".format(
        combinatoricsNotFibbonacciPosts))
if mostPopularMonth == 11:
    print("The most popular month for graph-theory posts is {} with {} posts".format(calendar.month_name[mostPopularMonth], maxAmountOfPostsInAMonth))
else:
    print("Wrong month for maximum posts: {} with {} posts".format(calendar.month_name[mostPopularMonth], maxAmountOfPostsInAMonth))

plt.show()

end = time.time()
print("Time elapsed: {:.2f}s".format(end - start))
