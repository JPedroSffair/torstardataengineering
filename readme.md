# Task Description #
We’ve provided a copy of the [Posts.xml](https://drive.google.com/file/d/1BHZPlsjt3_mo77jyACqbeYCV0v-T07-E/view) file (2.5G) from Math Stackexchange dated June 5 th (the entire
archive is available [here](https://archive.org/details/stackexchange)) . It contains approximately 2.3 million posts from math.stackexchange.com.
We would like you to process this data set and answer the following questions:
1. Count the number of posts made in June 2016.
2. Calculate the number of posts tagged with “combinatorics”.
    * For all posts tagged with “combinatorics” find the count of those not tagged with “fibonacci-numbers”.
3. Determine the month that was most popular for posts tagged with “graph-theory”
4. Create a timeseries plot of the number of “graph-theory” posts across all time captured in the dataset by quarter.

To run the program:
Create a `data` folder and download the `Posts.xml` file into it
Run the program

